<?php require_once  "./code.php";?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activities 1 and 2: PHP Basics and Selection Control structure</title>
    </head>

    <body>
        <h1>Activity 1</h1>
        <h2>Full Adress</h1>
        <p><?php echo $fullAddress1; ?></p>
        <p><?php echo $fullAddress2; ?></p>
        <p><?php echo $fullAddress3; ?></p>

        <h1>Activity 2</h1>
        <h2>Letter-Based Grading</h2>
        <p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
        <p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
        <p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>
    </body>
</html>