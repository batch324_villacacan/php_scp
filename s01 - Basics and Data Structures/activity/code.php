<?php 
//codes here

//ACTIVITY 01 Full Address
// Address 01
$houseNo = '154';
$street = 'Meralco Road';
$purok = 'purok 4B';
$barangay = 'Lower Bicutan';
$city = 'Taguig City';
$province = 'Metro Manila';
$country = 'Philippines';

$fullAddress1 = "{$houseNo} {$street}, {$purok}, {$barangay}, {$city}, {$province}, {$country}";

// Address 02
$floorNo = '3F';
$building = 'Caswyn Bldg.';
$street = 'Timog Avenue';
$city = 'Quezon City';

$fullAddress2 = "{$floorNo} {$building}, {$street}, {$city}, {$province}, {$country}";

// Address 03

$floorNo = '3F';
$building = 'Enzo Bldg.';
$street = 'Buendia Avenue';
$city = 'Makati City';

$fullAddress3 = "{$floorNo} {$building}, {$street}, {$city}, {$province}, {$country}";

//ACTIVITY 02 Letter-Based Grading
function getLetterGrade($numericalGrade){
    if($numericalGrade <75){
        return 'D';
    } else if($numericalGrade <=76) {
        return 'C-';
    } else if($numericalGrade <=79) {
        return 'C';
    } else if($numericalGrade <=82) {
        return 'C+';
    } else if($numericalGrade <=85) {
        return 'B-';
    } else if($numericalGrade <=88) {
        return 'B';
    } else if($numericalGrade <=91) {
        return 'B+';
    } else if($numericalGrade <=94) {
        return 'A-';
    } else if($numericalGrade <=97) {
        return 'A';
    } else if($numericalGrade <=100) {
        return 'A+';
    } else if($numericalGrade > 100) {
        return 'input less than or equal to 100';
    } else {
        return 'input a positive number';
    }
}

//codes end here
?>