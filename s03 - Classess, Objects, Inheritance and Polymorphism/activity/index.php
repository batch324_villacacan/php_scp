<?php require_once './code.php' ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S03 Activity - Polymorphism and Abstraction</title>
    </head>

    <body>
        <h1>S03 - Activity</h1>
        
        <h1>Person</h1>
        <p><?php echo $person->printName(); ?></p>

        <h1>Developer</h1>
        <p><?php echo $developer->printName(); ?></p>

        <h1>Engineer</h1>
        <p><?php echo $engineer->printName(); ?></p>

    </body>
</html>