<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <style>
        .login-form {
            display: flex;
            align-items: center;
        }

        .login-form input[type="text"],
        .login-form input[type="password"],
        .login-form input[type="submit"] {
            margin: 1em;
        }
    </style>
</head>
<body>
    <?php
    session_start();

    // Check if user is already logged in
    if (isset($_SESSION['email'])) {
        echo "Welcome, " . $_SESSION['email'] . "<br>";
        echo "
        <form method='post' action='logout.php'>
            <input type='submit' value='Logout'>
        </form>";
    } else {
        // Show login form
        echo "
        <form class='login-form' method='post' action='server.php'>
            <label for='email'>Email:</label>
            <input type='text' id='email' name='email'><br>
            <label for='password'>Password:</label>
            <input type='password' id='password' name='password'><br>
            <input type='submit' value='Login'>
        </form>";
    }
    ?>
</body>
</html>
