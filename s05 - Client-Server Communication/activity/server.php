<?php
session_start();

$validEmail = 'johnsmith@gmail.com';
$validPassword = '1234';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $email = $_POST['email'];
    $password = $_POST['password'];

    if ($email === $validEmail && $password === $validPassword) {
        $_SESSION['email'] = $email;
        header("Location: index.php");
    } else {
        echo "Invalid credentials. <a href='index.php'>Try again</a>";
    }
}
?>
