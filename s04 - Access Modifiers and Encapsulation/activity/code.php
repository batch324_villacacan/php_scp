<?php
// code starts here

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // Getter Function
    public function getName(){
        return $this->name;
    }

    // Setter Function
    public function setName($name){
        $this->name = $name;
    }

    public function printName(){
        return "The name of the building is $this->name.";
    }

    public function checkFloors(){
        return "The $this->name has $this->floors floors.";
    }

    public function checkAddress(){
        return "The $this->name is located at $this->address.";
    }

    public function nameChange(){
        return "The name of the building has been changed to $this->name.";
    }
    
}

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

class Condominium extends Building {}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
// code ends here
?>