<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S04 Activity - Access Modifiers and Encapsulation</title>
    </head>

    <body>
        <h1>Building</h1>
        <p><?php echo $building->printName(); ?></p>
        <p><?php echo $building->checkFloors(); ?></p>
        <p><?php echo $building->checkAddress(); ?></p>
        <p><?php $building->setName('Caswyn Complex'); ?></p>
        <p><?php echo $building->nameChange(); ?></p>

        <h1>Condominium</h1>
        <p><?php echo $condominium->printName(); ?></p>
        <p><?php echo $condominium->checkFloors(); ?></p>
        <p><?php echo $condominium->checkAddress(); ?></p>
        <p><?php $condominium->setName('Enzo Tower'); ?></p>
        <p><?php echo $condominium->nameChange(); ?></p>
    </body>
</html>
