<?php 
// code starts here

class Building {
    private $name;
    private $floors;
    private $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // getter function
    public function getName(){
        return $this->name;
    }

    // setter function
    public function setName($name){
        $this->name = $name;
    }

    public function getFloors(){
        return $this->floors;
    }
}

// getter function



// class Condominium extends Building{

// }

$building = new Building('Trial', 9, 'Manila City, NCR');
// $condominium = new Condominium('Megaworld', 68, 'Quezon City');

class Drink {
    protected $name;

    public function __construct($name){
        $this->name = $name;
    }
}

$milk = new Drink('Bearbrand');

class Coffee extends Drink {}

$kopiko = new Coffee('Kopiko');

// code ends here
?>