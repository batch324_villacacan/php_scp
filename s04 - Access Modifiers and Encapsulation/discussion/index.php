<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S04 - Access Modifiers and Encapsulation</title>
    </head>

    <body>
        <h1>S04 - Access Modifiers and Encapsulation</h1>
        <p><?php //$buiding->name = 'Caswyn Building'; ?></p>
        <p?<?php var_dump($building); ?></p>
        <p><?php echo $building->getName(); ?></p>
        <p><?php $building->setName('Caswyn'); ?></p>
        <p><?php echo $building->getName(); ?></p>
        <p><?php echo $building->getFloors(); ?> floors</p>

        <p><?php echo $milk->name; ?> floors</p>

        <p><?php var_dump($kopiko); ?></p>
    </body>
</html>