<?php 
//code starts
//[SECTION] Repetition Control Structures
    // It is used to execute code multiple times.

    //While Loop
        // A While loop takes a single condition

function whileLoop() {
    $count = 5;

    while($count !== 0){
        echo $count.'<br/>';
        $count--;
    }
}

function whileLoop1($count) {

    while($count !== 0){
        echo $count.'<br/>';
        $count--;
    }
}

    //Do-While Loop
        //A do-while loop works lot a like the While loop. The only difference is that, do-while guarantee that the code will run/executed at least once.

function doWhileLoop() {
    $count = 20;

    do {
        echo $count.'<br/>';
        $count--;
    } while ($count > 0);
}

    //For Loop
        /*
            for (inital value; condition; iteration){
                //code block ; 
            } 
        */

function forLoop() {
    for($count = 0; $count <= 20; $count++){
        echo $count.'<br/>';
    }
}

    //Continue and Break Statements

        // "Continue" is a keyword that allows the code to go the next loop without finishing the current code block
        // "Break" on the other hand is a keyword that stops the execution of the loop.

function modifiedForLoop(){
    for ($count = 0; $count <= 20; $count++){
        // If the count is divisible by 2, do this:
        if($count % 2 === 0){
            continue;
        }
        //If not, just continue the iteration
        echo $count.'<br/>';
        // If the count is greater than 10, do this:
        if($count > 10){
            break;
        }
    }
}

// [SECTION] Array Manipulation
    // An array is a kind of variable that can hold more than one value.
    // Arrays are delcared using array() function or square brackets '[]'.
        // In the earlier verision of php, we cannot use [], but as of PHP 5.4 we can use the shor array syntax which replace array() with [].

//Before the PHP 5.4
$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927');

//After PHP5.4
$studentNumbers = ['2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927'];

//Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'ASUS', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
$tasks =[
    'drink html',
    'eat php',
    'inhale css',
    'bake javascript'
];

// Associative Array
    // Associative Array differ from numeric array in the sense that associative arrays uses descriptive names in naming the elements/values (key)
    // Double Arrow Operator (=>) - an assignment operator that is commonly used in the creation of associatve array.

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-dimensional Array

$heroes = [
    ['ironman', 'thor', 'hulk'],
    ['wolverine', 'cylcops', 'jean grey'],
    ['batman', 'superman', 'wonder woman']
];

//Two-Dimensional Associative Array

$ironManPowers = [
    'regular' => ['repulsor blast', 'rocket punch'],
    'signature' => ['unibeam']
];

//Array iterative method: foreach();


//[Section] Array Mutators
//Array Mutators modify the contentes of an array.
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);


//code ends
?>