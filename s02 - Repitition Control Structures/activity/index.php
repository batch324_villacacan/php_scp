<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S02 Activity - Selection Control Structures and Array Manipulation</title>
    </head>

    <body>
        <h1>Activities</h1>

        <h2>Divisibles of 5</h2>
        <p><?php printFirst100DivisibleBy5(); ?></p>
        
        <h2>Array Manipulation</h2>
        <p><?php array_push($students, 'John Smith'); ?></p>
        <p><?php print_r($students); ?></p>
        <p><?php echo count($students); ?></p>

        <p><?php array_push($students, 'Jane Smith'); ?></p>
        <p><?php print_r($students); ?></p>
        <p><?php echo count($students); ?></p>

        <p><?php array_shift($students); ?></p>
        <p><?php print_r($students); ?></p>
        <p><?php echo count($students); ?></p>

    </body>
</html>